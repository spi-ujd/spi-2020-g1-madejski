# Wyświetla trzecie pole wiersza, jeśli piąte pole wiersza składa się z napisu "root". Zastosowano instrukcję warunkową if.

{
  if ( $5 ~ /root/ ) 
  {
  print $3
  }
}
