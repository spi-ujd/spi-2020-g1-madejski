# Wyświetlają się pierwsze, trzecie i czwarte pole wiersza, jeżeli wiersz nie zawiera napisu "matchme". Zastosowano instrukcję warunkową if.

{
  if ( $0 !~ /matchme/ ) 
  {
    print $1 $3 $4
  }
}
