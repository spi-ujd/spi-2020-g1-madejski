# Wyświetla pierwsze i trzecie pole podanego pliku, które są rozdzielone znakiem ":" oraz dodaje spację pomiędzy nimi.

awk -F":"

{ 
   print $1 " " $3
}
