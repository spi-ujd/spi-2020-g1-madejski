# W bloku BEGIN inicjowana jest zmienna x. Po napotkaniu w pliku pustego wiersza, zwiększana jest wartość x o 1. Po zakończeniu przetwarzania pliku, wykonuje się blok END - wypisywana jest wartość x, czyli liczba pustych wierszy. 

BEGIN 
{ 
  x = 0 
}
/^$/ 
{ 
  x = x + 1 
}
END 
{ 
  print x 
}
