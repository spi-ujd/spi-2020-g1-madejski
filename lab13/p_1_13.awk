# Przykład zastosowania zagnieżdżonej instrukcji warunkowej if. Jeśli pierwsze pole to napis "foo" i drugie to też "foo", to wyświetla "uno". Jeśli pierwsze pole to napis "foo" i drugie jest różne od "foo", to wyświetla "one". Jeśli pierwsze pole to napis "bar", to wyświetla "two". Jeśli pierwsze pole jest różne od napisu "foo" lub "bar", to wyświetla "three".

{
  if ( $1 == "foo" ) 
  {
    if ( $2 == "foo" ) 
    {
      print "uno"
    }
    else
    {
      print "one"
    }
  } 
  else if ($1 == "bar" ) 
  {
    print "two"
  } 
  else 
  {
    print "three"
  }
}
