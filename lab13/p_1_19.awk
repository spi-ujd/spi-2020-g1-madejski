# Wyświetlają się pierwsze, drugie i trzecie pole wiersza, jeżeli wiersz posiada więcej niż dwa pola. Pole pierwsze i drugie jest rozdzielone spacją, drugie i trzecie dwukropkiem. Separatorem pól FS jest dwukropek. Zastosowano instrukcję warunkową if, wskaźnik liczby pól NF i separator pól FS.

FS=":"
{
  if ( NF > 2 ) 
  {
    print $1 " " $2 ":" $3
  }
}

