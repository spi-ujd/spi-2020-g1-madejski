# Wyświetla pierwsze i trzecie pole podanego pliku, które są rozdzielone znakiem ":" oraz dodaje napisy "username: " i "\t\tuid:" pomiędzy nimi.

awk -F":"

{ 
   print "username: " $1 "\t\tuid:" $3
}
