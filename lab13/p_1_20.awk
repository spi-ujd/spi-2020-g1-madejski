# Wyświetla się napis "Linia więcej niż dziesiąta!" w linii od jedenastej aż do końca pliku. Zastosowano instrukcję warunkową if i wskaźnik numeru wiersza NR.

{
  if ( NR > 10 )
  {
    print "Linia więcej niż dziesiąta!"
  }
}

