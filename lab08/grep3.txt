42:/* Gather machine dependent type support.  */
143:    float_t	floating-point type at least as wide as `float' used
145:    double_t	floating-point type at least as wide as `double' used
268:#define __MATHDECL_VEC(type, function,suffix, args) \
270:  __MATHDECL(type, function,suffix, args)
274:#define __MATHDECL(type, function,suffix, args) \
275:  __MATHDECL_1(type, function,suffix, args); \
276:  __MATHDECL_1(type, __CONCAT(__,function),suffix, args)
279:#define __MATHDECLX(type, function,suffix, args, attrib) \
280:  __MATHDECL_1(type, function,suffix, args) __attribute__ (attrib); \
281:  __MATHDECL_1(type, __CONCAT(__,function),suffix, args) __attribute__ (attrib)
282:#define __MATHDECL_1(type, function,suffix, args) \
283:  extern type __MATH_PRECNAME(function,suffix) args __THROW
334:#   define __MATHDECL_2(type, function,suffix, args, alias) \
335:  extern type __REDIRECT_NTH(__MATH_PRECNAME(function,suffix), \
337:#   define __MATHDECL_1(type, function,suffix, args) \
338:  __MATHDECL_2(type, function,suffix, args, __CONCAT(function,suffix))
486:/* Declare functions returning a narrower type.  */
785:/* Depending on the type of TG_ARG, call an appropriately suffixed
790:   does not have a real floating type.  The definition may use a
792:   return the same type (FUNC may include a cast if necessary rather
849:/* ISO C99 defines some generic macros which work on any data type.  */
983:      the correct parameter (regardless of type qualifiers (i.e.: const
1250:/* An expression whose type has the widest of the evaluation formats
1271:   which need not have the same type.  Choosing what underlying function
1274:   information, however, only the type of the macro expansion is
1276:   Thus, the type is used as a template parameter for __iseqsig_type,
