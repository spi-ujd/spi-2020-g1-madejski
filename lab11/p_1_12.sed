# Otwiera plik mojplik.txt, wczytuje wiersz z pliku, następnie wykonuje operację zamieniania występującego słowa "wzgórza" na słowo "góry" w liniach rozpoczynających się pustą linią i kończących się słowem "END" włącznie.

sed -e '/^$/,/^END/s/wzgórza/góry/g' mojplik.txt

