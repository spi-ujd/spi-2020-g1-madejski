# Otwiera plik mojplik.txt, wczytuje wiersz z pliku, następnie wykonuje operację zamieniania wszystkich występujących słów "zaczarowanie" na słowo "zablokowanie". Wykonuje wymienione operacje dla wierszy od nr 1 do nr 10 w pliku.

sed -e '1,10s/zaczarowanie/zablokowanie/g' mojplik.txt

