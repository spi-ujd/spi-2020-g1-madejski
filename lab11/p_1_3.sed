# Otwiera plik /etc/services, wczytuje wiersz z pliku, następnie wykonuje operację "-d" usuwania wiersza, na końcu wypisuje zawartość wiersza, czyli pusty wiersz. Wykonuje wymienione operacje dla wierszy od nr 1 do nr 10 w pliku.

sed -e '1,10d' /etc/services
