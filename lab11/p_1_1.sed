# Otwiera plik /etc/services, wczytuje wiersz z pliku, następnie wykonuje operację "-d" usuwania wiersza, na końcu wypisuje zawartość wiersza, czyli pusty wiersz. Wykonuje wymienione operacje dla wszystkich wierszy w pliku.

sed -e 'd' /etc/services
