44:## Do case-sensitive searches by default.
45:# set casesensitive
222:## 'color' will do case-sensitive matches, while 'icolor' will do
223:## case-insensitive matches.
