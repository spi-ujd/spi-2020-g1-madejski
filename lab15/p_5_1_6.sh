#!/bin/bash

ext=$1

for filename in `ls *.$ext`
do
     tmp=`basename $filename $ext`
     newname=`echo $tmp | tr '[a-z]' '[A-Z]'`
     mv $filename $newname$ext
done
