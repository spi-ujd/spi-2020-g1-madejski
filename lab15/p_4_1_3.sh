#!/bin/bash

if [ "$#" -gt 2 ]
then
     echo "Za duzo argumentow"
     echo "Wywolanie: $0 nazwa_pliku [nazwa_katalogu]"
     exit 1
fi

if [ "$#" = "0" ]
then
     echo "Nie podales nazwy pliku"
     echo "Wywolanie: $0 nazwa_pliku [nazwa_katalogu]"
     exit 2
fi

if [ ! -e "$1" ]
then
     echo "Taki plik nie istnieje"
     exit 3
fi

if [ -n "$2" ]
then
     if [ ! -d "$2" ]
     then
          mkdir "$2"
     fi
     cp "$1" "$2"/
else
     if [ ! -d "default" ]
     then
          mkdir "default"
     fi
     cp "$1" default/
fi

if [ "$?" = "0" ]
then
     echo "Skopiowano"
else
     echo "Nie skopiowano"
fi
