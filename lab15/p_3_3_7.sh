#!/bin/bash

cd ~
cd $OLDPWD
echo "Sciezka do Twojego katalogu domowego to: $HOME"
echo "Jestes zalogowany jako: $USER"
echo "Lista katalogow z plikami wykonywalnymi: $PATH"
echo "Uzywany emulator terminala: $TERM"
echo "Znajdujesz sie w katalogu: $PWD"
echo "Poprzedni katalog, w ktorym byles to: $OLDPWD"
